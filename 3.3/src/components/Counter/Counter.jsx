import React from "react";
import './Counter.css';

class Counter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      active: 0,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    const step = 1;
    const elementTitle = e.target.innerHTML;
    console.log(elementTitle);
    const result = elementTitle === "Increment" ? this.state.value + step : this.state.value - step;

    this.setState(() => {
      return {
        value: result,
      };
    });
  }

  render() {
    return (
      <div className="count-wrap">
        <div className="count-box"> Count: {this.state.value}</div>
        <button className="btn btn-increment" onClick={this.handleClick}>
          Increment
        </button>
        <button className="btn btn-decrement" onClick={this.handleClick}>
          Decrement
        </button>
      </div>
    );
  }
}

export default Counter;
