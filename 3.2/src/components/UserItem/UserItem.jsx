import React from "react";
import users from "../../users";
import "./UserItem.css";

class UserItem extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    // const user = users.map((user) => <li>{user.name}</li>);
    return (
      <li>
        <div className="user-card-inner">
          <div className="user-card-name">{this.props.name}</div>
          <div className="user-card-job">{this.props.job}</div>
        </div>
      </li>
    );
  }
}

export default UserItem;
