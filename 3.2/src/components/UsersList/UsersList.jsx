import React from "react";
import uuid from 'react-uuid';
import users from '../../users';
import UserItem from '../UserItem/UserItem';
import './UsersList.css'

class UsersList extends React.Component {
  constructor(props) {
    super(props);
  }

  render(){

    const user = users.map((user) => (
        <UserItem key={uuid()} name={user.name} job={user.job}/>
    ))

      return <ul>{user}</ul>
  }
}

export default UsersList;
