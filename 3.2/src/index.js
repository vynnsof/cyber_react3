import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import users from './users';
import UsersList from './components/UsersList';

ReactDOM.render(
  <React.StrictMode>
    <h1>Users List</h1>
    <UsersList users={users}/>
  </React.StrictMode>,
  document.getElementById('root')
);

