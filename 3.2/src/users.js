const users = [
    {
      name: "Charlie",
      job: "Janitor",
    },
    {
      name: "Mac",
      job: "Bouncer",
    },
    {
      name: "Dee",
      job: "Aspring actress",
    },
    {
      name: "Dennis",
      job: "Bartender",
    },
    {
      name: "Liam",
      job: "Doctor",
    },
    {
      name: "Olivia",
      job: "QA",
    },
    {
      name: "Noah",
      job: "Product Owner",
    },
    {
      name: "Emma",
      job: "Police officer",
    }	
  ];

  export default users